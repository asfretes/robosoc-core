package roboSoc.Core;

public class CircuitBreakerPosicionador implements ICircuitBreaker {

	private IServicioRemoto _servicio;
	private long _tiempoReintento;
	private int _erroresPermitidos;
	
	private int _estado;
	private int _contErrores;
	private int _contReintentos;
	private long _tsUltimoError;
	private String _ultimoError;
	
	public CircuitBreakerPosicionador(IServicioRemoto servicio, long nsReintento, int erroresPermitidos) {
		super();
		this._servicio = servicio;
		this._tiempoReintento = nsReintento * 1000000000;
		this._erroresPermitidos = erroresPermitidos;
		
		this._estado=1;
		this._tsUltimoError = System.nanoTime() + (long)(1000.00 * 1000.00 * 1000.00 * 1000.00);
		this._contErrores=0;
		this._contReintentos=0;
		this._ultimoError="";
	}

	@Override
	public Object invocarServicio(Object param) throws Exception 
	{
		actualizarEstado();
		Object o=null;
		
		/*Si contin�o en estado abierto, genero una excepcion*/
		if(this._estado==0) {
			long tActual = System.nanoTime();
			long tFaltante = this._tiempoReintento - (tActual - this._tsUltimoError);
					
			this._ultimoError="Se alcanz� el limite de errores, reintentar en " + String.format("%.2f",((double)tFaltante/1000000000)) + "Seg." + " (Reintentos : " + this._contReintentos + ")" ;
			Exception e = new Exception(this._ultimoError);
			throw e;
		}
	
		/*Si el estado no es abierto, ejecuto el servicio*/
		try {								
			o = this._servicio.solicitud(param);
			registrarExito();		
			return o; 
			
		} catch (Exception e) {
			if(this._estado==2) this._contReintentos++;
			
			registrarError(e);
			throw e;
		}
		

	}

	
	private void registrarExito()
	{
		this._estado=1;
		this._tsUltimoError = System.nanoTime() + (long)(1000.00 * 1000.00 * 1000.00 * 1000.00);
		this._contErrores=0;
		this._contReintentos=0;
		this._ultimoError="";
		System.out.println("\n\n");
	}
	
	private void registrarError(Exception e)
	{
		this._contErrores++;
		this._tsUltimoError=System.nanoTime();
		
		this._ultimoError= "ERRORES: " + this._contErrores + " - ULTIMO: "+ e.getMessage();
		
	}
	
	private void actualizarEstado()
	{
		if (this._contErrores >= this._erroresPermitidos) { 
		      if ((System.nanoTime() - this._tsUltimoError) > this._tiempoReintento) {
		        this._estado=2;
		      } else {
		    	this._estado=0;
		      }
		    } else {
		    	this._estado=1;
		    }
		
	}

	@Override
	public String ultimoError() {
		// TODO Auto-generated method stub
		return String.valueOf(this._ultimoError);
	}
	
	
}
