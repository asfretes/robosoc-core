package roboSoc.Core;

public interface IPosicionable{

	double get_posX();
	void set_posX(double _posX);

	double get_posY();
	void set_posY(double _posY);

	double get_posZ();
	void set_posZ(double _posz);
	
}