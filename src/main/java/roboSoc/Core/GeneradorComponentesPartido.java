package roboSoc.Core;

import java.util.LinkedList;

public class GeneradorComponentesPartido implements IGeneradorComponentes {

	IBuscadorComponentes _buscador;
	
	public GeneradorComponentesPartido(IBuscadorComponentes buscador) {
		super();
		this._buscador = buscador;
	}

	@Override
	public Object generarInstancia(String className) {
		if(this._buscador==null) return null;
		
		LinkedList<Class> disponibles = new LinkedList<Class>();
		disponibles = (LinkedList<Class>) this._buscador.buscarComponente(className);
		
		if(disponibles.size()==0) return null;
		
		Object o = null;
		
		o = instanciar(disponibles.get(0));
		
		return o;
	}


	public static Object instanciar(Class clase) {
		Object o;
		/*siempre devuelvo una instancia del primer elemento encontrado.*/
		try {
			o=clase.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			o=null;
		}
		return o;
	}
	
}
