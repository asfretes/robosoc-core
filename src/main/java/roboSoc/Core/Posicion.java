package roboSoc.Core;

public class Posicion implements IPosicionable {
	private double _x;
	private double _y;
	private double _z;
	
	public Posicion()
	{
		this._x=0;
		this._y=0;
		this._z=0;
	}

	@Override
	public double get_posX() {
		return this._x;
	}

	@Override
	public void set_posX(double posX) {
		this._x=posX;
	}

	@Override
	public double get_posY() {
		return this._y;
	}

	@Override
	public void set_posY(double posY) {
		this._y=posY;
	}

	@Override
	public double get_posZ() {
		return this._z;
	}

	@Override
	public void set_posZ(double posZ) {
		this._z=posZ;
	}

}
