package roboSoc.Core;

import java.io.Serializable;

public class RobotDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private double _posX;
	private double _posY;
	private double _posZ;

	public RobotDTO(double _posX, double _posY, double _posZ) {
		super();
		this._posX = _posX;
		this._posY = _posY;
		this._posZ = _posZ;
	}
	
	public RobotDTO() {
		super();
		this._posX = 0;
		this._posY = 0;
		this._posZ = 0;
	}
	
	public double get_posX() {
		return _posX;
	}
	public void set_posX(double _posX) {
		this._posX = _posX;
	}
	public double get_posY() {
		return _posY;
	}
	public void set_posY(double _posY) {
		this._posY = _posY;
	}
	public double get_posZ() {
		return _posZ;
	}
	public void set_posZ(double _posZ) {
		this._posZ = _posZ;
	}	
		
}
