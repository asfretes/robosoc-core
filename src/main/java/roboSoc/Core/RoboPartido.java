package roboSoc.Core;

import java.util.ArrayList;

public class RoboPartido extends Partido {
	private ICircuitBreaker _posicionador;
	
	public RoboPartido(int NroJugadores, ICircuitBreaker posicionador) {
		super();
		this._posicionador=posicionador;	
		this.inicializarEquipos(NroJugadores);		
	}

	private void inicializarEquipos(int NroJugadores) {
		ArrayList<IJugador> local=new ArrayList<IJugador>(NroJugadores);
		ArrayList<IJugador> visitante=new ArrayList<IJugador>(NroJugadores);
		
		Robot r;
		
		for(int x=0; x<NroJugadores;x++)
		{
			r=new Robot();
			r.set_Id(x);
			local.add(r);
			r=new Robot();
			r.set_Id(x);
			visitante.add(r);
		}
		
		this.set_equipoLocal(local);
		this.set_equipoVisitante(visitante);
	}
	
	public void actualizarPosiciones()
	{
		if(this._posicionador==null) return;
		Posicion p = null;
		ArrayList<IJugador> equipo=this.get_equipoLocal();			
		for(IJugador j:equipo)
		{
			try {
				p = (Posicion) this._posicionador.invocarServicio((Integer) j.get_Id());
				j.set_posX(p.get_posX());
				j.set_posY(p.get_posY());
				j.set_posZ(p.get_posZ());
			} catch (Exception e) {
				p=null;
			}
			
		}
		
		equipo=this.get_equipoVisitante();
		for(IJugador j:equipo)
		{
			try {
				p=(Posicion) this._posicionador.invocarServicio((Integer) j.get_Id()+equipo.size());
				j.set_posX(p.get_posX());
				j.set_posY(p.get_posY());
				j.set_posZ(p.get_posZ());
			} catch (Exception e) {
				p=null;
			}
		}
		
		notificar();
	}

	public ArrayList<RobotDTO> equipoLocal()
	{
		ArrayList<RobotDTO> equipo = new ArrayList<RobotDTO>();
		
		for(IJugador a:this.get_equipoLocal())
		{
			equipo.add(new RobotDTO(a.get_posX(),a.get_posY(),a.get_posZ()));
			
		}
 		return equipo;
	}
	

	public ArrayList<RobotDTO> equipoVisitante()
	{
		ArrayList<RobotDTO> equipo = new ArrayList<RobotDTO>();
		
		for(IJugador a:this.get_equipoVisitante())
		{
			equipo.add(new RobotDTO(a.get_posX(),a.get_posY(),a.get_posZ()));
		}
 		
		return equipo;
	}

	public void set_posicionador(ICircuitBreaker _posicionador) {
		this._posicionador = _posicionador;
	}
	
	public String estadoServicios()
	{
		return this._posicionador.ultimoError();
	}
}
