package roboSoc.Core;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public abstract class Partido implements IObservable
{
	private Set<IObservador> _observadores;
	
	private ArrayList<IJugador> _equipoLocal;
	private ArrayList<IJugador> _equipoVisitante;
	
	private IPosicionable _pelota;

	public Partido()
	{
		this._observadores=new HashSet<IObservador>();
	}
	@Override
	public void agregarObservador(IObservador obs) {
		_observadores.add(obs);	
	}

	@Override
	public void quitarObservador(IObservador obs) {
		_observadores.remove(obs);	
	}

	@Override
	public void notificar() {
		for (IObservador iObservador : _observadores) {
			iObservador.actualizar();
		}
	}

	protected IPosicionable get_pelota() {
		return _pelota;
	}

	protected void set_pelota(IPosicionable _pelota) {
		this._pelota = _pelota;
	}

	protected ArrayList<IJugador> get_equipoVisitante() {
		return _equipoVisitante;
	}

	protected void set_equipoVisitante(ArrayList<IJugador> _equipoVisitante) {
		this._equipoVisitante = _equipoVisitante;
	}

	protected ArrayList<IJugador> get_equipoLocal() {
		return _equipoLocal;
	}

	protected void set_equipoLocal(ArrayList<IJugador> _equipoLocal) {
		this._equipoLocal = _equipoLocal;
	}

}
