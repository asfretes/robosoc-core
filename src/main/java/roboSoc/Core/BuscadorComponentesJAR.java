package roboSoc.Core;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class BuscadorComponentesJAR implements IBuscadorComponentes {

private String _Jar;

public BuscadorComponentesJAR(String rutaJar) {
	super();
	this._Jar=rutaJar;
}


public static URL linkearJAR(String jar)
{
	URL urlcl=null;
	try {
		urlcl = new URL(jar); //("jar:file:" + _Jar + "!/");
	} catch (MalformedURLException e1) {
		/*La ruta al JAr es incorrecta*/
		urlcl=null;
	}
	return urlcl;
}

public List<Class> buscarComponente(String className) {
	
//	System.out.println("---->>> Busco : " + className);
	
	Class findClass = null;
	List<Class> result = new LinkedList<Class>();

	
	try {
		findClass = Class.forName(className);
	} catch (ClassNotFoundException e) {
		/*La clase solicitada no se encuentra definida*/
		findClass=null;
	}
	
	//if(this._Jar.equals("")) return result;
		
	URL urlcl=linkearJAR("jar:file:" + _Jar + "!/");
	
	/*null;
	try {
		urlcl = new URL("jar:file:" + _Jar + "!/");
	} catch (MalformedURLException e1) {

		return result; }*/
	
	URL[] urls = {urlcl };
	URLClassLoader cl = URLClassLoader.newInstance(urls);
	
	//System.out.println("---->>> Jar ok = " + urls);
	
	//System.out.println("---->>> Recorro ZIP : ");
	
	ZipInputStream zip;
	try {
		zip = new ZipInputStream(new FileInputStream(""+_Jar+""));
	
		for (ZipEntry itemZip = zip.getNextEntry(); itemZip != null; itemZip = zip.getNextEntry()) {
		    if (!itemZip.isDirectory() && itemZip.getName().endsWith(".class")) {

		    	String fileName = itemZip.getName().substring(0,itemZip.getName().length()-6);
		    	fileName = fileName.replace('/', '.');
		    	
		    	Class fileClass = cl.loadClass(fileName);
		    	Object o = fileClass.newInstance();		    	
		    	
				//if (findClass.isInstance(o) || findClass.getClass().equals(o.getClass()))
		    	if((findClass==null && o.getClass().getName()==className) 
		    			|| (findClass!=null && findClass.isInstance(o)) )
		    	{
					result.add(fileClass);
				}
	
		    }
		}
		
		zip.close();
		
	} catch (IOException | ClassNotFoundException  | InstantiationException  | IllegalAccessException | ClassFormatError e) {
		//e.printStackTrace();
		return result;//new HashSet<IPosicionador>();
	}

	return result;
}

public String get_rutaJar() {
	return _Jar;
}

public void set_rutaJar(String _rutaJar) {
	this._Jar = _rutaJar;
}



/*
 * public Set<Class> buscarPosicionadores() {
	//System.out.println("---->>> Working Directory = " + System.getProperty("user.dir"));
	//System.out.println("---->>> Jar buscado = " + this._Jar);
	Set<Class> result = new HashSet<Class>();
	
	if(this._Jar.equals("")) return result;
		
	URL urlcl=null;
	try {
		urlcl = new URL("jar:file:" + _Jar + "!/");
	} catch (MalformedURLException e1) {
		//e1.printStackTrace();
		//System.out.println("---->>> Jar erroneo  = " + "jar:file:" + _Jar + "!/");
		return result;
	}
	
	URL[] urls = {urlcl };
	URLClassLoader cl = URLClassLoader.newInstance(urls);
	
	//System.out.println("---->>> Jar ok = " + urls);
	
	ZipInputStream zip;
	try {
		zip = new ZipInputStream(new FileInputStream(""+_Jar+""));
	
		for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
		    if (!entry.isDirectory() && entry.getName().endsWith(".class")) {

		    	String className = entry.getName().substring(0,entry.getName().length()-6);
		    	className = className.replace('/', '.');
		    	
		    	Class c = cl.loadClass(className);
		    	Object o = c.newInstance();
		    	
				//(if (c.isInstance(IPosicionador.class))
				if(o instanceof IPosicionador )
		    	{
					result.add(c);
				}
	
		    }
		}
		
		zip.close();
		
	} catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
		//e.printStackTrace();
		return result;//new HashSet<IPosicionador>();
	}

	return result;
}
 * 
 * 
 * */






}



