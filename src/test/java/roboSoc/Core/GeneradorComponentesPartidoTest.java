package roboSoc.Core;

import static org.junit.Assert.*;

import org.junit.Test;

public class GeneradorComponentesPartidoTest {

	@Test
	public void testGenerarInstancia() {
		BuscadorComponentesJAR bus = new BuscadorComponentesJAR("test/robosoc-pos-1.jar");
		
		IGeneradorComponentes generaComp = new GeneradorComponentesPartido(bus);
		
		Object o = generaComp.generarInstancia("roboSoc.Core.IPosicionador");
		
		assertTrue(o==null);
	}
	

	@Test
	public void testGenerardor1() {
		BuscadorComponentesJAR bus = null;
		
		IGeneradorComponentes generaComp = new GeneradorComponentesPartido(bus);
		
		Object o = generaComp.generarInstancia("");
		assertTrue(o==null);
		
	}
	
	private class Prueba
	{
		private int _i;
		private Prueba(int i)
		{
			this._i=i;
		}
	}
	
	
	@Test
	public void testinstanciar() {		
		Object o = GeneradorComponentesPartido.instanciar(Prueba.class);
		assertTrue(o==null);
	}
	
	
	
	
}
