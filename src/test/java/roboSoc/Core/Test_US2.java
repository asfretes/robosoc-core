package roboSoc.Core;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class Test_US2 {

	private class Servicio1 implements IServicioRemoto
	{
		int _solicitudes;
		public Servicio1() {
			super();
			this._solicitudes = 0;
		}
		@Override
		public Object solicitud(Object param) throws Exception {
			this._solicitudes++;
			return "Servicio OK.";
		}
		
	}
	
	private class Servicio2 implements IServicioRemoto
	{
		int _solicitudes;
		public Servicio2() {
			super();
			this._solicitudes = 0;
		}
		@Override
		public Object solicitud(Object param) throws Exception {
			this._solicitudes++;
					
			if(this._solicitudes>1 && this._solicitudes<=3)
			{
				Exception e = new Exception("Error Solicitud " + (this._solicitudes));
				throw e;
			}
			
			return "Servicio OK.";
				
		}
		
	}
	
	private class Servicio3 implements IServicioRemoto
	{
		int _solicitudes;
		public Servicio3() {
			super();
			this._solicitudes = 0;
		}
		@Override
		public Object solicitud(Object param) throws Exception {
			this._solicitudes++;
					
			if(this._solicitudes<=3)
			{
				Exception e = new Exception("Error Solicitud " + (this._solicitudes));
				throw e;
			}
			
			return "Servicio OK.";
				
		}
		
	}
	
	private class Servicio4 implements IServicioRemoto
	{
		int _solicitudes;
		public Servicio4() {
			super();
			this._solicitudes = 0;
		}
		@Override
		public Object solicitud(Object param) throws Exception {
			this._solicitudes++;
					
			if(this._solicitudes>0)
			{
				Exception e = new Exception("Error Solicitud " + (this._solicitudes));
				throw e;
			}
			
			return "Servicio OK.";
				
		}
		
	}
	
	@Test
	public void testCriterioDeAceptacion1() {
		Servicio1 s1=new Servicio1() ;
		
		CircuitBreakerPosicionador cb=new CircuitBreakerPosicionador(s1, 1, 2);
		int intentos=4;
		ArrayList<String> respuestas = new ArrayList<String>();
		
		for(int i=0; i<intentos; i++)
		{
			try {
				respuestas.add((String) cb.invocarServicio(""));
			} catch (Exception e) {
				//si da algun error, falla, porque en este CA no debe tenerlo.
				assertTrue(false);
			}
		}
		
		assertTrue(respuestas.get(0).equals("Servicio OK.")
					&& respuestas.get(1).equals("Servicio OK.") 
					&& respuestas.get(2).equals("Servicio OK.") 
					&& respuestas.get(3).equals("Servicio OK."));
		
	}

	
	@Test
	public void testCriterioDeAceptacion2() {
		Servicio2 s=new Servicio2() ;
		
		CircuitBreakerPosicionador cb=new CircuitBreakerPosicionador(s, 1, 2);
		int intentos=4;
		ArrayList<String> respuestas = new ArrayList<String>();
		String respuesta="";
		for(int i=0; i<intentos; i++)
		{
			try {
				respuesta = (String) cb.invocarServicio("");
				respuestas.add(respuesta);
			} catch (Exception e) {
				//assertTrue(false);
				respuestas.add(e.getMessage());
			}
		}
		
		assertTrue(respuestas.get(0).equals("Servicio OK.")
					&& respuestas.get(1).equals("Error Solicitud 2") 
					&& respuestas.get(2).equals("Error Solicitud 3") 
					&& !respuestas.get(3).equals("Servicio OK."));
		
	}
	
	
	@Test
	public void testCriterioDeAceptacion3() {
		Servicio2 s=new Servicio2() ;
		
		CircuitBreakerPosicionador cb=new CircuitBreakerPosicionador(s, 1, 2);
		int intentos=4;
		ArrayList<String> respuestas = new ArrayList<String>();
		String respuesta="";
		for(int i=0; i<intentos; i++)
		{
			try {
				
				if(i==3)					
				{
					/*Espero un segundo*/
					try{ 
						Thread.sleep((long) (1000)); 
					} catch(InterruptedException e ) {};
				}	
				
				respuesta = (String) cb.invocarServicio("");
				respuestas.add(respuesta);
				
			} catch (Exception e) {
				//assertTrue(false);
				respuestas.add(e.getMessage());
			}
		}
		
		assertTrue(respuestas.get(0).equals("Servicio OK.")
					&& respuestas.get(1).equals("Error Solicitud 2") 
					&& respuestas.get(2).equals("Error Solicitud 3") 
					&& respuestas.get(3).equals("Servicio OK."));
		
	}
	
	@Test
	public void testCriterioDeAceptacion4() {
		Servicio3 s=new Servicio3() ;
		
		CircuitBreakerPosicionador cb=new CircuitBreakerPosicionador(s, 1, 2);
		int intentos=4;
		ArrayList<String> respuestas = new ArrayList<String>();
		String respuesta="";
		for(int i=0; i<intentos; i++)
		{
			try {
				
				if(i>=2)					
				{
					/*Espero un segundo*/
					try{ 
						Thread.sleep((long) (1000)); 
					} catch(InterruptedException e ) {};
				}	
				
				respuesta = (String) cb.invocarServicio("");
				respuestas.add(respuesta);
				
			} catch (Exception e) {
				//assertTrue(false);
				respuestas.add(e.getMessage());
			}
		}
		
		assertTrue( !respuestas.get(0).equals("Servicio OK.")
					&& !respuestas.get(1).equals("Servicio OK.") 
					&& !respuestas.get(2).equals("Servicio OK.") 
					&& respuestas.get(3).equals("Servicio OK."));
		
	}
	
	

}
