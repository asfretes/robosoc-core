package roboSoc.Core;

import static org.junit.Assert.*;

import java.net.URL;
import java.util.LinkedList;

import org.junit.Test;

public class BuscadorComponentesTest {

	@Test
	public void testBuscarPosicionadoresJar() {
	
		BuscadorComponentesJAR gestor=new BuscadorComponentesJAR("test/robosoc-pos-4.jar");
		LinkedList<Class> posicionadores;
		posicionadores=(LinkedList<Class>) gestor.buscarComponente("roboSoc.Core.IPosicionador");
		assertTrue(posicionadores.size()==2);
	}
	
	public void testBuscarPosicionadoresJar1() {
		
		BuscadorComponentesJAR gestor=new BuscadorComponentesJAR("test/robosoc-pos-4.jar");
		LinkedList<Class> posicionadores;
		posicionadores=(LinkedList<Class>) gestor.buscarComponente("roboSoc.Pos.PosicionadorRandom");
		assertTrue(posicionadores.size()==1);
	}
	
	
	@Test
	public void testBuscarPosicionadoresJarPathErr() {
		BuscadorComponentesJAR gestor=new BuscadorComponentesJAR("test/robosoc-pos-1.jar");
		LinkedList<Class> posicionadores;
		posicionadores=(LinkedList<Class>) gestor.buscarComponente("roboSoc.Core.IPosicionador");
		assertTrue(posicionadores.size()==0);
	}
	
	@Test
	public void testget_rutaJar() {
	
		BuscadorComponentesJAR gestor=new BuscadorComponentesJAR("TEST");
		assertTrue(gestor.get_rutaJar().equals("TEST"));
	}
	
	@Test
	public void testset_rutaJar() {
		BuscadorComponentesJAR gestor=new BuscadorComponentesJAR("");
		gestor.set_rutaJar("TEST");
		assertTrue(gestor.get_rutaJar().equals("TEST"));
	}
	
	@Test
	public void testGestorComponentes() {
		BuscadorComponentesJAR gestor=new BuscadorComponentesJAR("TEST");
		
		assertTrue(gestor!=null && gestor.get_rutaJar().equals("TEST"));
	}
	
	@Test
	public void testlinkearJARErr() {
		URL url = null;
		url = BuscadorComponentesJAR.linkearJAR("hh:/");
		
		assertTrue(url==null);
	}
	
	@Test
	public void testlinkearJAROk() {
		//BuscadorComponentesJAR
		URL url = null;
		url = BuscadorComponentesJAR.linkearJAR("jar:file:test!/");
		
		assertTrue(url!=null);
	}
	
	
}
