package roboSoc.Core;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Test;

public class RoboPartidoTEST {

	class PosTest implements IPosicionadorRemoto
	{

		@Override
		public IPosicionable getPosicion(int id) {
			Posicion p = new Posicion();
			p.set_posX(id);
			p.set_posY(id);
			p.set_posZ(id);
			return p;
		}

		@Override
		public Object solicitud(Object param) throws Exception {
			
			return getPosicion((int) param);
		}
		
	}
	
	
	class PosTestErr implements IPosicionadorRemoto
	{

		@Override
		public IPosicionable getPosicion(int id) {
			Posicion p = new Posicion();
			p.set_posX(id);
			p.set_posY(id);
			p.set_posZ(id);
			return p;
		}

		@Override
		public Object solicitud(Object param) throws Exception {
			
			Exception e = new Exception("Error al consultar posiciones");
			throw e;
		}
		
	}
	
	
	@Test
	public void testRoboPartido() {
		int jugadores=2;
		CircuitBreakerPosicionador cb = new CircuitBreakerPosicionador(new PosTest(), 1000000, 1);
		RoboPartido partido = new RoboPartido(jugadores, cb);
		
		assertTrue(partido!=null && partido.equipoLocal().size()==jugadores 
				&& partido.equipoVisitante().size()==jugadores);
				
	}

	@Test
	public void testEquipoLocal() {
		int jugadores=2;
		CircuitBreakerPosicionador cb = new CircuitBreakerPosicionador(new PosTest(), 1000000, 1);
		RoboPartido partido = new RoboPartido(jugadores, cb);

		ArrayList<RobotDTO> equipo=null;
		
		equipo=partido.equipoLocal();
		
		assertTrue(equipo!=null && equipo.size()==jugadores);
				
	}

	@Test
	public void testEquipoVisitante() {
		int jugadores=2;
		CircuitBreakerPosicionador cb = new CircuitBreakerPosicionador(new PosTest(), 1000000, 1);
		RoboPartido partido = new RoboPartido(jugadores, cb);
		
		ArrayList<RobotDTO> equipo=null;
		
		equipo=partido.equipoVisitante();
		
		assertTrue(equipo!=null && equipo.size()==jugadores);
						
	}
	
	
	@Test
	public void testactualizarPosiciones() {
		int jugadores=2;
		CircuitBreakerPosicionador cb = new CircuitBreakerPosicionador(new PosTest(), 1000000, 1);
		RoboPartido partido = new RoboPartido(jugadores, cb);

		ArrayList<RobotDTO> equipo=null;
		
		partido.actualizarPosiciones();
		
		equipo=partido.equipoLocal();
		
		RobotDTO r = equipo.get(0);
		
		assertTrue(r.get_posX()==0);
						
	}
	
	
	@Test
	public void testactualizarPosicionesErr() {
		int jugadores=2;
		CircuitBreakerPosicionador cb = new CircuitBreakerPosicionador(new PosTestErr(), 1000000, 1);
		RoboPartido partido = new RoboPartido(jugadores, cb);

		ArrayList<RobotDTO> equipo=null;
		
		partido.actualizarPosiciones();
		
		equipo=partido.equipoLocal();
		
		RobotDTO r = equipo.get(0);
		
		assertTrue(r.get_posX()==0);
						
	}
	
	
	
	@Test
	public void testset_posicionador() {
		int jugadores=2;
		CircuitBreakerPosicionador cb = new CircuitBreakerPosicionador(new PosTest(), 1000000, 1);
		RoboPartido partido = new RoboPartido(jugadores, cb);

		ArrayList<RobotDTO> equipo=null;
		
		partido.set_posicionador(null);
		partido.actualizarPosiciones();
		
		equipo=partido.equipoLocal();
		
		RobotDTO r = equipo.get(0);
		assertTrue(r.get_posX()==0);
						
	}
	

}
